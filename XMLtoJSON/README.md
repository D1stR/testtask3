# Тестовое задание "Джейсон Иксэмэл" #

## Функционал 💻👩‍💻🕙📆
- Переводит ваш XML файл в формат JSON
- Приложение содержит 2 формы:

- форму, через которую можно загрузить файл на сервер(обязательно xml),
- форму, где можно увидеть результат загрузки.

## Как проверить
- Запустить приложение, настроив под себя путь папки на сервер и PostgreSQL
- Перейти на URL: http://localhost/files
***
- После того как вы загрузите файл, он перенаправит вас на URL: http://localhost/files/json/${fileStorageName}
- Нажмите на кнопку "Преобразовать JSON" и получите результат
- Сам этот файл открывается по URL: http://localhost/files/json/${fileStorageName}/response (Если интересно)

## Используемый стек технологий 💻📱🧑🏖

- Java 17
- [Spring Boot](https://spring.io/projects/spring-boot/)
- [PostgreSQL](https://www.postgresql.org)
- [Lombok](https://projectlombok.org/)
- [Freemarker](https://freemarker.apache.org/index.html)
- [Jackson](https://github.com/FasterXML/jackson-docs)
- [SpringBootTest](https://docs.spring.io/spring-boot/docs/current/api/org/springframework/boot/test/context/SpringBootTest.html)




## Демонстрация проекта 💻📱📍🗺

- [Видео-демонстрация](https://youtu.be/mFVqjxTc-f0)
***
![](https://sun9-74.userapi.com/impg/EDqRsHai_1rXwaLFRqlcQ2YKQ6lrhhMYXRsR9w/yi5pzikOZr8.jpg?size=1896x1000&quality=96&sign=2e9356f18d385adb26f60315abf1324c&type=album)
***
![](https://sun9-78.userapi.com/impg/PrAful4O7V6940gU4WtATj11_sOntM2Xbrg1DQ/J_9Od2AHdOo.jpg?size=1500x766&quality=96&sign=c80cffcb2adf3976f3db6da03154f745&type=album)
***
![](https://sun36-1.userapi.com/impg/LP4rZ8eENb_Qdm5pxQ9lUSWWzrvZa2fhOG8Btw/HCw8hL4F5JQ.jpg?size=1815x888&quality=96&sign=40471e1de35e0ad84f27b508af041e51&type=album)
***
[Telegram создателя 💬🐤](https://t.me/D1stRRR)
***
