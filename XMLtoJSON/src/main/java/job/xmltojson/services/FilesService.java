package job.xmltojson.services;

import jakarta.servlet.http.HttpServletResponse;
import job.xmltojson.models.FileInfo;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;

public interface FilesService {
    FileInfo upload(MultipartFile multipart, String description);

    void addFileToResponse(String fileName, HttpServletResponse response) throws IOException;
}

