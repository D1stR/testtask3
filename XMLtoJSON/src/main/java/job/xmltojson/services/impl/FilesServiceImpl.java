package job.xmltojson.services.impl;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectWriter;
import com.fasterxml.jackson.dataformat.xml.XmlMapper;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.transaction.Transactional;
import job.xmltojson.exceptions.FileMustBeXMLException;
import job.xmltojson.exceptions.FileNotFoundException;
import job.xmltojson.models.FileInfo;
import job.xmltojson.repositories.FilesInfoRepository;
import job.xmltojson.services.FilesService;
import lombok.RequiredArgsConstructor;
import org.apache.tomcat.util.http.fileupload.IOUtils;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Objects;
import java.util.UUID;

@RequiredArgsConstructor
@Service
@PropertySource(value = {"application-${spring.profiles.active}.properties"})
public class FilesServiceImpl implements FilesService {

    @Value("${files.storage.path}")
    private String storagePath;

    @Value("${files.url}")
    private String filesUrl;

    private final FilesInfoRepository filesInfoRepository;

    @Transactional
    @Override
    public FileInfo upload(MultipartFile multipart, String description) {
        try {
            String extension = Objects.requireNonNull(multipart.getOriginalFilename()).substring(multipart.getOriginalFilename().lastIndexOf("."));
//only xml upload
            String storageFileName = UUID.randomUUID() + extension;
            if (multipart.getContentType().contains("xml")) {
                FileInfo file = FileInfo.builder()
                        .type(multipart.getContentType())
                        .originalFileName(multipart.getOriginalFilename())
                        .description(description)
                        .storageFileName(storageFileName)
                        .size(multipart.getSize())
                        .build();

                Files.copy(multipart.getInputStream(), Paths.get(storagePath, file.getStorageFileName()));


                filesInfoRepository.save(file);

                File xmlFile = new File("D:\\SSD\\IdeaProjects\\XMLtoJSON\\files\\" + storageFileName);
                File jsonFile = new File("D:\\SSD\\IdeaProjects\\XMLtoJSON\\files\\" + storageFileName + ".json");

                try {
                    // Создаем XML Mapper
                    XmlMapper xmlMapper = new XmlMapper();

                    // Читаем XML файл и преобразуем его в JsonNode
                    JsonNode jsonNode = xmlMapper.readTree(xmlFile);

                    // Создаем ObjectMapper для форматирования JSON
                    ObjectMapper jsonMapper = new ObjectMapper();
                    ObjectWriter writer = jsonMapper.writerWithDefaultPrettyPrinter();

                    // Преобразуем JsonNode в форматированную JSON строку
                    String json = writer.writeValueAsString(jsonNode);
                    System.out.println(json);
                    // Записываем JSON  в файл
                    jsonMapper.writeValue(jsonFile, jsonNode);

                    System.out.println("Конвертация выполнена успешно!");

                } catch (IOException e) {
                    System.out.println("Ошибка при конвертации XML в JSON: " + e.getMessage());
                }
                return file;
            } else {
                throw new FileMustBeXMLException("File must be xml!");
            }

        } catch (IOException e) {
            throw new IllegalArgumentException(e);
        }

    }

    @Override
    public void addFileToResponse(String fileName, HttpServletResponse response) {
        FileInfo file = filesInfoRepository.findByStorageFileName(fileName).orElseThrow(FileNotFoundException::new);

        response.setContentLength(file.getSize().intValue());
        response.setContentType("application/json");
        response.setHeader("Content-Disposition", "filename=\"" + file.getOriginalFileName() + "\"");
        response.setHeader("File-Description", file.getDescription());
        try {
            IOUtils.copy(new FileInputStream(storagePath + "\\" + file.getStorageFileName() + ".json"), response.getOutputStream());
            response.flushBuffer();

        } catch (IOException e) {
            throw new IllegalArgumentException(e);
        }

    }


}
