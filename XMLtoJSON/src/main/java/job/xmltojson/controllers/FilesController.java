package job.xmltojson.controllers;

import jakarta.servlet.http.HttpServletResponse;
import job.xmltojson.models.FileInfo;
import job.xmltojson.services.FilesService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.view.RedirectView;

import java.io.IOException;

@RequiredArgsConstructor
@Controller
@RequestMapping("/files")
public class FilesController {

    private final FilesService filesService;

    @GetMapping()
    public String getFilesUploadPage(Model model) {
        return "file_upload_page";
    }

    @PostMapping(value = "/upload")
    @ResponseBody
    public RedirectView upload(@RequestParam("file") MultipartFile multipart, @RequestParam("description") String description) {
        FileInfo fileInfo = filesService.upload(multipart, description);
        return new RedirectView("/files/json/" + fileInfo.getStorageFileName());
    }

    @GetMapping("/json/{file-name:.+}")
    public String getFile(@PathVariable("file-name") String fileName, HttpServletResponse response, Model model) throws IOException {
        model.addAttribute("file", fileName);
        return "json_page";
    }

    @GetMapping("/json/{file-name:.+}/response")
    public void getFile(@PathVariable("file-name") String fileName, HttpServletResponse response) throws IOException {
        filesService.addFileToResponse(fileName, response);
    }
}

