package job.xmltojson.validation;

import lombok.Builder;
import lombok.Data;
import lombok.RequiredArgsConstructor;

import java.util.List;

@Data
@RequiredArgsConstructor
@Builder
public class ValidationErrorResponse {
    private final List<Violation> violations;
}

